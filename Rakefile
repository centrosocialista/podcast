covers = Dir['content/*/cover.svg'].inject([]) do |list,item|
  list << item.sub('.svg', '.png') << item.sub('.svg', '-large.png')
end

def inkscape(*args)
  cmd = ['inkscape'] + args
  puts cmd.join(' ')
  system(*cmd, out: "/dev/null") or fail
end

rule '.png' => ->(f) { f.sub(%r{(-large)?\.png}, '.svg') } do |t|
  params = (t.name =~ /-large.png/) && ['--export-width=720'] || []
  inkscape '--export-type=png', "--export-filename=#{t.name}", *params, t.source
end

rule '.jpg' => ->(f) { f.sub(%r{\.jpg}, '.svg') } do |t|
  inkscape "--export-type=png", "--export-filename=#{t.name}.png", '--export-width=720', '--export-height=720', t.source
  sh 'convert', "#{t.name}.png", t.name
  rm_f "#{t.name}.png"
end

desc 'builds the site'
task :default => covers do
  sh 'nanoc'
end

desc 'adds a new episode'
task :new do
  require 'erubis'
  last = Dir.chdir('content') { Dir['[0-9]*/index.md'] }.map { |d| File.dirname(d).to_i }.sort.last || 0
  episode = last + 1
  new = File.join('content', episode.to_s)
  mkdir_p new
  cp 'util/cover.svg', new
  date = Time.now.strftime('%Y-%m-%d')
  index = File.join(new, 'index.md')
  template = Erubis::Eruby.new(File.read('util/template.erb'))
  File.open(index, 'w') do |f|
    f.write(template.result(date: date, episode: episode))
  end
  puts 'created %s' % index
end

desc 'Runs a local server'
task :server => :default do
  sh 'nanoc', 'live'
end
