require 'pathname'
class AudioDataSource < ::Nanoc::DataSource
  identifier :audio

  SOURCES_ROOT = File.expand_path(File.dirname(__FILE__) + "/../..")

  def items
    source = Pathname(__FILE__).parent.parent.parent.glob("content/[0-9]*/index.md")
    fake_data = File.join(SOURCES_ROOT, "data")
    FileUtils.mkdir_p(fake_data)
    source.map do |f|
      data = YAML.load_file(f)
      data["audio"].map do |audio|
        filename = audio["filename"]
        source = get_source(filename)
        link = File.join(SOURCES_ROOT, "tmp", File.basename(source))
        FileUtils.touch(File.join(fake_data, filename))
        FileUtils.mkdir_p(File.dirname(link))
        FileUtils.ln_sf source, link
        new_item(link, {}, "/#{f.dirname.basename}/#{filename}", binary: true)
      end
    end.flatten
  end

  def get_source(filename)
    if ENV['NANOC_ENV'] == "production"
      "../data/#{filename}"
    else
      "#{SOURCES_ROOT}/util/sonar#{File.extname(filename)}"
    end
  end
end
