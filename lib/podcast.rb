include Nanoc::Helpers::Blogging
include Nanoc::Helpers::Rendering
include Nanoc::Helpers::LinkTo

class Audio
  def initialize(data, item)
    path = File.dirname(item.identifier).sub(%r{^/}, '')
    filename = data[:filename]
    @filesize = data[:filesize]
    @filename = File.join(path, filename)
    @filepath = File.join('content', @filename)
    @duration_s = item[:duration]
    fail '%s does not specify episode duration!' % item.identifier unless @duration_s
  end

  def url
    '/' + @filename
  end

  def download
    File.basename(@filename)
  end

  def duration
    @duration ||=
      begin
        s = @duration_s.round
        h = s / 3600
        m = (s % 3600) / 60
        s = s - (h*3600) - (m*60)
        '%02d:%02d:%02d' % [h, m, s]
      end
  end

  def length
    @filesize
  end

  def size
    length / 1024**2
  end
end

def audio_formats
  @audio_formats ||= @config[:audio_formats]
end

def audio(episode, format)
  @audio ||= {}
  @audio[format + ':' + episode.identifier] ||= __get_episode_audio__(episode, format)
end

def __get_episode_audio__(episode, format)
  files = episode[:audio].select { |f| f[:filename] =~ /\.#{format}$/ }
  case files.size
  when 0
    fail('Episode %s contains no %s file' % [episode.identifier, format])
  when 1
    :OK
  else
    fail('Episode %s contains more than one %s file' % [episode.identifier, format])
  end
  Audio.new(files[0], episode)
end

def cover_path(episode, variant = nil)
  cover = variant && "cover-#{variant}.png" || "cover.png"
  relative_path_to(File.join(File.dirname(episode.identifier), cover))
end

def date(episode)
  Time.parse(episode[:created_at]).strftime('%d/%m/%Y')
end

def rfc2822(episode)
  Time.parse(episode[:created_at]).rfc2822
end
