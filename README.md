# Debate Socialista

## Como construir o site localmente

Requisitos:

* Ruby
* nanoc 4 (apt-get install nanoc no debian 12+, ou via rubygems)
* inkscape

Tarefas comuns:

| Para  | Faça |
| ------| ---- |
| construir o site | `rake` |
| servir a versão local do site | `rake server` |
| acessar a versão local so site | abra [127.0.0.1:3000](http://127.0.0.1:3000) |
| criar um novo episódio | `rake new` |
