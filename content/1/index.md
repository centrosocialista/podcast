---
kind: article
created_at: "2024-01-26"
title: "#1 - Alysson Mascaro"
audio:
- filename: papcs-1-alysson-mascaro.mp3
  filesize: 55338766
duration: 4102
short_description: >
  Alysson Mascaro: as dificuldades e conquistas dos últimos anos, e as
  perspectivas da luta pela revolução brasileira.

---

O camarada professor Alysson Mascaro é um pensador dos campos do Direito e da
Filosofia. Alysson é o intelectual responsável pela ideia fundamental dos
centros socialistas. Neste episódio, passaremos pelas dificuldades e pelas
conquistas dos últimos anos, e as perspectivas da luta pela revolução
brasileira.

Créditos:

- Edição: Joselice Abreu
- Locução da introdução e do encerramento: Ana Carolina Alves
- Trilha sonora: [https://freemusicarchive.org/music/mr-smith/fever-dreams-1/your-drowning-in-it/](Your Drowning In It, por Mr Smith).
  - Licença: Creative Commons Attribution (CC-BY) 4.0.
- Capa: Aurélio Heckert
